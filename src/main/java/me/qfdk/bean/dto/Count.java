package me.qfdk.bean.dto;

import java.math.BigDecimal;

public class Count {

    private BigDecimal amount;

    private BigDecimal alipay;

    private BigDecimal wechat;

    private BigDecimal qq;

    private BigDecimal union;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getAlipay() {
        return alipay;
    }

    public void setAlipay(BigDecimal alipay) {
        this.alipay = alipay;
    }

    public BigDecimal getWechat() {
        return wechat;
    }

    public void setWechat(BigDecimal wechat) {
        this.wechat = wechat;
    }

    public BigDecimal getQq() {
        return qq;
    }

    public void setQq(BigDecimal qq) {
        this.qq = qq;
    }

    public BigDecimal getUnion() {
        return union;
    }

    public void setUnion(BigDecimal union) {
        this.union = union;
    }
}
